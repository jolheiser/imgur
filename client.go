package imgur

import (
	"context"
	"fmt"
	"io"
	"net/http"
)

var baseURL = "https://api.imgur.com/3/"

// Client is an Imgur client
type Client struct {
	id   string
	http *http.Client
}

// New creates a new Client
func New(clientID string, opts ...ClientOption) *Client {
	c := &Client{
		id:   clientID,
		http: http.DefaultClient,
	}
	for _, opt := range opts {
		opt(c)
	}
	return c
}

// ClientOption is option for a Client
type ClientOption func(*Client)

// WithHTTP sets the http.Client for a Client
func WithHTTP(client *http.Client) ClientOption {
	return func(c *Client) {
		c.http = client
	}
}

func (c *Client) newRequest(ctx context.Context, method, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequestWithContext(ctx, method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", fmt.Sprintf("Client-ID %s", c.id))
	return req, nil
}
