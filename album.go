package imgur

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

// Album is an imgur album
type Album struct {
	ID          string   `json:"id"`
	Title       string   `json:"title"`
	Description string   `json:"description"`
	Datetime    int      `json:"datetime"`
	Cover       string   `json:"cover"`
	CoverWidth  int      `json:"cover_width"`
	CoverHeight int      `json:"cover_height"`
	AccountURL  string   `json:"account_url"`
	AccountID   int      `json:"account_id"`
	Privacy     string   `json:"privacy"`
	Layout      string   `json:"layout"`
	Views       int      `json:"views"`
	Link        string   `json:"link"`
	Favorite    bool     `json:"favorite"`
	NSFW        bool     `json:"nsfw"`
	Section     string   `json:"section"`
	Order       int      `json:"order"`
	DeleteHash  string   `json:"deletehash"`
	ImagesCount int      `json:"images_count"`
	Images      []*Image `json:"images"`
	InGallery   bool     `json:"in_gallery"`
}

// Album returns an imgur.Album
func (c *Client) Album(ctx context.Context, hash string) (*Album, error) {
	req, err := c.newRequest(ctx, http.MethodGet, fmt.Sprintf("%salbum/%s", baseURL, hash), nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}

	data := response{
		Data: &Album{},
	}
	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return nil, err
	}

	if !data.Success {
		return nil, fmt.Errorf("imgur returned non-ok status: %d", data.Status)
	}

	return data.Data.(*Album), nil
}

// AlbumImages returns the images in an Album
func (c *Client) AlbumImages(ctx context.Context, hash string) ([]*Image, error) {
	req, err := c.newRequest(ctx, http.MethodGet, fmt.Sprintf("%salbum/%s/images", baseURL, hash), nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}

	data := response{
		Data: []*Image{},
	}
	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return nil, err
	}

	if !data.Success {
		return nil, fmt.Errorf("imgur returned non-ok status: %d", data.Status)
	}

	return data.Data.([]*Image), nil
}

// AlbumImage returns an image in an Album
func (c *Client) AlbumImage(ctx context.Context, albumHash, imageHash string) (*Image, error) {
	req, err := c.newRequest(ctx, http.MethodGet, fmt.Sprintf("%salbum/%s/image/%s", baseURL, albumHash, imageHash), nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}

	data := response{
		Data: &Image{},
	}
	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return nil, err
	}

	if !data.Success {
		return nil, fmt.Errorf("imgur returned non-ok status: %d", data.Status)
	}

	return data.Data.(*Image), nil
}
