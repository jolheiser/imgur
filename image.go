package imgur

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

// Image is an imgur image
type Image struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Datetime    int    `json:"datetime"`
	Type        string `json:"type"`
	Animated    bool   `json:"animated"`
	Width       int    `json:"width"`
	Height      int    `json:"height"`
	Size        int    `json:"size"`
	Views       int    `json:"views"`
	Bandwidth   int    `json:"bandwidth"`
	DeleteHash  string `json:"deletehash"`
	Name        string `json:"name"`
	Section     string `json:"section"`
	Link        string `json:"link"`
	GIFV        string `json:"gifv"`
	MP4         string `json:"mp4"`
	MP4Size     int    `json:"mp4_size"`
	Looping     bool   `json:"looping"`
	Favorite    bool   `json:"favorite"`
	NSFW        bool   `json:"nsfw"`
	Vote        string `json:"vote"`
	InGallery   bool   `json:"in_gallery"`
}

// Image returns an imgur.Image
func (c *Client) Image(ctx context.Context, hash string) (*Image, error) {
	req, err := c.newRequest(ctx, http.MethodGet, fmt.Sprintf("%simage/%s", baseURL, hash), nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}

	data := response{
		Data: &Image{},
	}
	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return nil, err
	}

	if !data.Success {
		return nil, fmt.Errorf("imgur returned non-ok status: %d", data.Status)
	}

	return data.Data.(*Image), nil
}
